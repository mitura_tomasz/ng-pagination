import { Component } from '@angular/core';
import { PaginationService } from './services/pagination.service';
import { PaginationLimit } from './models/pagination-limit';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [PaginationService]
})
export class AppComponent {
  
  paginationLimits: PaginationLimit;
  numbers: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];

  constructor(private _testsPaginationService: PaginationService) {}

  ngOnInit() {
    this._testsPaginationService.getPaginationLimits().subscribe(resPaginationLimits => {
      this.paginationLimits = resPaginationLimits
    }); 
  }
}
