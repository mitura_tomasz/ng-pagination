import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PaginationModule } from './components/pagination/pagination.module';
import { EntitiesSelectModule } from './components/entities-select/entities-select.module';
import { PaginationService } from './services/pagination.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PaginationModule,
    EntitiesSelectModule
  ],
  providers: [PaginationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
