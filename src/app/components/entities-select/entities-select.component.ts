import { Component, OnInit, Input } from '@angular/core';
import { EntitiesDisplayOptions } from '../../models/entities-display-options';
import { PaginationService } from '../../services/pagination.service';


@Component({
  selector: 'app-entities-select',
  templateUrl: './entities-select.component.html',
  styleUrls: ['./entities-select.component.scss']
})
export class EntitiesSelectComponent implements OnInit {

  entitiesDisplayOptions: EntitiesDisplayOptions[];
  numOfShownEntities: number;

  constructor(private _testsPaginationService: PaginationService) { }

  ngOnInit() {
    this.entitiesDisplayOptions = this._testsPaginationService.createEntitiesDisplayOptions();

    this._testsPaginationService.getNumOfShownEntities().subscribe(resNumOfShownEntities => {
      this.numOfShownEntities = resNumOfShownEntities;
    })
  }

  onNumOfShownEntitiesChange(selectedNumOfShownEntities: number): void {
    // this.paginationTabs = this.createTableOfPaginationTabs();   
    
    this._testsPaginationService.setNumOfShownEntities(selectedNumOfShownEntities);
    this._testsPaginationService.setActivePaginationTab(1);
    this.updatePaginationLimits();
  }

  updatePaginationLimits(): void {
    let paginationLimits = {
      lower: 0,
      upper: this.numOfShownEntities
    }

    this._testsPaginationService.setPaginationLimits(paginationLimits);
  }

}
