import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntitiesSelectComponent } from './entities-select.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [EntitiesSelectComponent],
  exports: [
    EntitiesSelectComponent
  ]
})
export class EntitiesSelectModule { }
