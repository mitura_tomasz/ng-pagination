import { Component, OnInit, Input } from '@angular/core';
import { PaginationLimit } from '../../models/pagination-limit';
import { PaginationService } from '../../services/pagination.service';


@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  activePaginationTab: number;
  paginationLimits: PaginationLimit;
  @Input() numOfEntities: number;
  numOfShownEntities: number;
  numOfvisibleAdjacentTabs: number = 1;


  constructor(private _testsPaginationService: PaginationService) { }

  ngOnInit() {
    this._testsPaginationService.getActivePaginationTab().subscribe(resActivePaginationTab => {
      this.activePaginationTab = resActivePaginationTab;
    });

    this._testsPaginationService.getPaginationLimits().subscribe(resPaginationLimits => {
      this.paginationLimits = resPaginationLimits
    });

    this._testsPaginationService.getNumOfShownEntities().subscribe(resNumOfShownEntities => {
      this.numOfShownEntities = resNumOfShownEntities;
    })
  }


  getNumberOfPaginationTabs(): number {
    let numOfShownEntities = this.paginationLimits.upper - this.paginationLimits.lower;
    return Math.ceil(this.numOfEntities / numOfShownEntities);
  }

  /*
    Funkcja tworzay tablice z wartosciami liczbowymi które maja byc przypisane kolejnym
    przyciskom paginacji
  */
  createTableOfPaginationTabs(): number[] {
    let paginationTab = [];
    let numOfPaginationTabs = this.getNumberOfPaginationTabs();

    for (let i = 0; i < numOfPaginationTabs; ++i) {
      paginationTab.push(i + 1);
    }
    return paginationTab;
  }

  updatePaginationLimits(): void {
    let paginationLimits = {
      lower: ((this.activePaginationTab - 1) * this.numOfShownEntities),
      upper: (this.activePaginationTab * this.numOfShownEntities),
    }

    this._testsPaginationService.setPaginationLimits(paginationLimits);
  }
  
  /*
    Funkcja sprawdza czy przekazana w parametrze podstrona paginacji znajduje sie w sasiedztwie
    aktywnej podstrony paginacji, gdzie zasieg widzialnych podstron jest ustalony w obiekcie paginationLimits
  */
  isVisibleAdjacentTab(tabIndex: number): boolean {
    let minVisibleTabs = 4;

    if (this.activePaginationTab < minVisibleTabs && tabIndex < minVisibleTabs) {
      return true;
    }

    if (
      tabIndex > this.getNumberOfPaginationTabs() - minVisibleTabs - 1 && 
      this.activePaginationTab > (this.getNumberOfPaginationTabs() - minVisibleTabs + 1)
    ) {
      return true;
    }

    let isMoreThanLowerLimit = (tabIndex + 2) > (this.activePaginationTab - this.numOfvisibleAdjacentTabs)
    let isLessThanUpperLimit = tabIndex < (this.activePaginationTab + this.numOfvisibleAdjacentTabs) 

    return isMoreThanLowerLimit && isLessThanUpperLimit; 
  }

  onPaginationFirstClick(): void {
    this.activePaginationTab = 1;
    this.updatePaginationLimits();
  }
  
  onPaginationPreviousClick(): void {
    if (this.activePaginationTab > 1) {
      --this.activePaginationTab;
      
      this.updatePaginationLimits();
    }
  }
  
  onPaginationTabClick(activePaginationTab: number): void {
    this.activePaginationTab = activePaginationTab;
    this.updatePaginationLimits();
  }

  onPaginationNextClick(): void {
    if (this.activePaginationTab < this.getNumberOfPaginationTabs()) {
      ++this.activePaginationTab;
      this.updatePaginationLimits();
    }
  }

  onPaginationLastClick(): void {
    this.activePaginationTab = this.getNumberOfPaginationTabs();
    this.updatePaginationLimits();
  }
      
}
    