export interface EntitiesDisplayOptions {
  value: number,
  text: string
}