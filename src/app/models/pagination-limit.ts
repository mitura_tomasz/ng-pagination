export interface PaginationLimit {
  lower: number,
  upper: number
}