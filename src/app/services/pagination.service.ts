import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { EntitiesDisplayOptions } from '../models/entities-display-options';
import { PaginationLimit } from '../models/pagination-limit';

@Injectable()
export class PaginationService {
  
  entitiesDisplayOptions: EntitiesDisplayOptions[] = this.createEntitiesDisplayOptions();
  paginationLimits$: Subject<PaginationLimit> = new BehaviorSubject<PaginationLimit>(this.createPaginationLimits());
  numOfShownEntities$: Subject<any> = new BehaviorSubject( this.createEntitiesDisplayOptions()[0].value );
  activePaginationTab$: Subject<number> = new BehaviorSubject<number>(1);

  constructor() { }


  createPaginationLimits(): any {
    return {
      lower: 0,
      upper: this.entitiesDisplayOptions[0].value
    }
  }

  createEntitiesDisplayOptions(): EntitiesDisplayOptions[] {
    return [
      { value: 2, text: '2' },
      { value: 4, text: '4' },
      { value: 8, text: '8' },
      { value: 9999999, text: 'All' }
    ];
  }

  getPaginationLimits(): Observable<PaginationLimit> {
    return this.paginationLimits$.asObservable()
  }
  
  setPaginationLimits(value: any): void {
    this.paginationLimits$.next(value);
  }

  getEntitiesDisplayOptions(): EntitiesDisplayOptions[] {
    return this.entitiesDisplayOptions;
  }

  getNumOfShownEntities(): Observable<number> {
    return this.numOfShownEntities$.asObservable();
  }
  setNumOfShownEntities(value: number): void {
    this.numOfShownEntities$.next(value);
  }

  getActivePaginationTab(): Observable<number> {
    return this.activePaginationTab$.asObservable();
  }
  setActivePaginationTab(value: number): void {
    this.activePaginationTab$.next(value);
  }

}
